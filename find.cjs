function find(elements, cb) {
    if (!elements === undefined || !Array.isArray(elements) || elements.length === 0) {
        return elements
    }
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index],index)) {
            return elements[index];
        }
    }
    return undefined;
}
module.exports = find;  
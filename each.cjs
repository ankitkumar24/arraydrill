function each(elements, cb) {
    let testArr=[]
    if (elements === undefined || !Array.isArray(elements) || elements.length === 0) {
      return testArr;
    }
    
    for (let index = 0; index < elements.length; index++) {
      cb(elements[index], index, elements);
    }
  }
  
  module.exports = each;
  
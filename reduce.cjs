function reduce(elements, cb, acc) {
    if (elements === undefined || !Array.isArray(elements) || elements.length === 0) {
      return [];
    }
  
   
  
    if (typeof cb !== 'function') {
      return [];
    }
  
   
    for (let index = 0; index < elements.length; index++) {
        if(acc===undefined){
            acc=elements[index]
            continue
          }
      acc = cb(acc, elements[index], index, elements);
    }
    return acc;
  }
  
  module.exports = reduce;
  
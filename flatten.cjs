

// function flatten(elements, depth) {
//     let testArr = [];
//     if (depth === undefined) {
//         return elements;
//     }

//     if (elements === undefined || !Array.isArray(elements) || elements.length === 0) {
//         return testArr;
//     }
//     if (depth < 1) {
//         return elements;
//     }

//     let flattenArr = [];
//     // flattenArr = elements.flat(depth)

//     for (let index = 0; index < elements.length; index++) {
//         if (elements[index] !== undefined && Array.isArray(elements[index])) {
//             const subArr = flatten(elements[index], depth - 1);
//             flattenArr = flattenArr.concat(subArr);
//         } else if (elements[index] !== undefined) {
//             flattenArr.push(elements[index]);
//         }
//     }

//     return flattenArr;
// }
function flatten(elements, depth = 1) {
    if (depth === 0 || !Array.isArray(elements)) {
      return elements;
    }
  
    let flattenArr = [];
  
    for (let index = 0; index < elements.length; index++) {
      if (Array.isArray(elements[index]) && elements[index] !== null && typeof elements[index] !== 'undefined') {
        const subArr = flatten(elements[index], depth - 1);
        flattenArr.push(...subArr);
      } else if (typeof elements[index] !== 'undefined') {
        flattenArr.push(elements[index]);
      }
    }
  
    return flattenArr;
  }
  
  module.exports = flatten;
  



module.exports = flatten;
;

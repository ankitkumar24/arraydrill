function filter(elements, cb) {
    let testArr = []
    if (elements === undefined || !Array.isArray(elements) || elements.length === 0) {
        return testArr;
    }
    const arr = [];
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements)===true) {
            arr.push(elements[index]);
        }
    }
    return arr;
}

module.exports = filter;
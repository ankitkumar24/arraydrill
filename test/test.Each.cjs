let each = require('../each.cjs');
const items = [1, 2, 3, 4, 5, 5];

each(items, (elements, index) => {
    console.log('Element is ' + elements + ' at index ' + index);
})
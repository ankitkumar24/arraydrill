function map(elements, cb) {
    let testArr = []
    if (elements === undefined || !Array.isArray(elements) || elements.length === 0) {
        return testArr
    }
    const mappedArr = [];
    for (let index = 0; index < elements.length; index++) {
        mappedArr.push(cb(elements[index], index,elements));
    }
    return mappedArr;
}

module.exports = map;